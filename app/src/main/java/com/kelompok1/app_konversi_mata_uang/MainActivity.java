package com.kelompok1.app_konversi_mata_uang;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    EditText input_uang;
    Button rp_usd, rp_euro, rp_yen;
    TextView hasil_konversi;

    double angka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input_uang = (EditText) findViewById(R.id.uang_input);
        rp_usd = (Button) findViewById(R.id.rpusd);
        rp_euro = (Button) findViewById(R.id.rpeuro);
        rp_yen = (Button) findViewById(R.id.rpyen);
        hasil_konversi = (TextView) findViewById(R.id.hasil_konversi);
    }


    public boolean cek(){
        if (input_uang.getText().toString().isEmpty()){
            Toast.makeText(this, "Silahkan Masukan Nominal Uang [Rp] ", Toast.LENGTH_SHORT).show();
            String falid = "Hasil Konversi";
            hasil_konversi.setText(falid);
            return false;
        }
        return true;
    }

    public void toYEN(View v){
        if (!cek()){
            return;
        } try{
            angka = Double.parseDouble(input_uang.getText().toString());
            double hasil = angka / 109;
            hasil_konversi.setText(NumberFormat.getCurrencyInstance(Locale.JAPAN).format(hasil));
            Toast.makeText(this, "1 Yen = Rp 109", Toast.LENGTH_SHORT).show();

        }catch(Exception ex){
            Toast.makeText(this, "Mohon Untuk Memasukkan Angka", Toast.LENGTH_SHORT).show();
            String falid = "Hasil Konversi";
            hasil_konversi.setText(falid);
        }
    }

    public void toEuro(View v){
        if (!cek()){
            return;
        } try{
            angka = Double.parseDouble(input_uang.getText().toString());
            double hasil = angka / 15703;
            hasil_konversi.setText(NumberFormat.getCurrencyInstance(Locale.GERMANY).format(hasil));
            Toast.makeText(this, "1 Euro = Rp 15.703", Toast.LENGTH_SHORT).show();

        }catch(Exception e){
            Toast.makeText(this, "Mohon Untuk Memasukkan Angka", Toast.LENGTH_SHORT).show();
            String falid = "Hasil Konversi";
            hasil_konversi.setText(falid);
        }
    }


    public void toUSD(View v){
        if (!cek()){
            return;
        }try{
            angka = Double.parseDouble(input_uang.getText().toString());
            double hasil = angka / 14830;
            hasil_konversi.setText(NumberFormat.getCurrencyInstance(Locale.US).format(hasil));
            Toast.makeText(this, "1 U$D = Rp 14830", Toast.LENGTH_SHORT).show();

        }catch(Exception e){
            Toast.makeText(this, "Mohon Untuk Memasukkan Angka", Toast.LENGTH_SHORT).show();
            String falid = "Hasil Konversi";
            hasil_konversi.setText(falid);
        }

    }
}




