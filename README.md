# >> Applikasi Konverter Mata Uang
------
Pada __README__ ini yaitu membuat __Applikasi Konverter Mata Uang__ ini.Dengan ketentuan sebagai berikut untuk:
* Applikasi Konverter Mata Uang (Kelompok 1) mengunakan Android Studio
* Menggunakan Bahasa Pemograman Java
* Merupakan Tugas Akhir untuk mata kuliah PBO 2

# >> Angota Kelompok 1
------
* Heri Fajar Nugroho
* Mohamad Rizky
* Mohammad Arsyal Ardiansyah
* Muhammad Fajroel Rachman
* R Zidan Sholaha

# >> Requirements
------
* java jdk 16.0.1
* Android API level minimum 25

# >> Installation
------
* Download Project dengan menggunakan Zip
* Copy Project Zip kedalam file yang bernama AndroidStudioProjects
* Pisahkan file yang .pptx, .pdf, .docx, dan .png (Untuk menghindari Error)
* Buka dan jalankan Project -> Dengan Open file pada applikasi Android Studio
* Buka app >> src >> MainActivity.java >> Run Project

# >> Preview
------
<img src = "Hasil.png" width = "300">


